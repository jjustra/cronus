# cronus

CRON in User Space. Simple cron server for all your user needs ;) Can run GUI programs easily. Is written in python.

Program will load all given tab files (see usage), then it begins a loop:
* check every rule - run commands of succeeded ones
* check files for a changes - reload all if it occur
* wait 60 seconds
* loop from beginning


## usage

just call : cronus [[-f] tab_file_path0 [tab_file_path1 [...]]] [-r "rule0" ["rule1"]] [-t]


### arguments

* -r - run following arguments as rules (switch to rule mode)
* -f - following arguments are file paths (switch to file_path mode)
* -t - print first 10 times every rule will apply in a year (test mode)

### examples

``cronus ~/.cronustab`` will load rule file ``~/.cronustab``

``cronus -r "* * * * * echo 1"`` will load rule, that echoes '1' every minute


## tab files

On every line you supply a rule consisting of five time parts, followed by command.

e.g. ``* * * * * echo 1`` (this will echo '1' every minute)

Empty lines are ignored as well as lines started with ``#`` (those are considered comments).


Parts are :
* minute (0 - 59)
* hour (0 - 23)
* day of month (1 - 28/30/31)
* month (1 - 12)
* day of week (0 - 6, 0 is monday)

You can input values in parts as :
* ``*`` - for 'any' or 'every' (every minute, hour, ...)
* ``-`` - for a range (e.g. ``1-3``)
* ``,`` - for a list (e.g. ``1,2,3``)
* ``#`` - concrete numeric values (e.g. ``1``, ``2``, ``3``)

You can also use modulo divider (``/``), to express modulo operation on above means. Some examples :
* ``*/2`` as hour would means every even hour (0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 and 22)
* ``*/3`` as hour would means every third hour (0, 3, 6, 9, 12, 15, 18 and 21)
* ``1-6/3`` is same as ``3,6``
* ``3/10`` as minute would means every tenth minute plus 3 (3, 13, 23, 33, 43 and 53)


Tip : To lock some time part on exact value, you must fill all time parts up to that one.

e.g. if you want set only hour, you must also fill minute (zero is good enought) - like in examle 02a
.. otherwise it will run on every minute in those hours - like in example - 02b


### example 01 :

``* * * * * echo 1``

means : run every minute


### example 02a :

``0 1-3 * * * echo 1``

means : run every ZERO minute of hours 1, 2 and 3 .. so it will run three times :
* 1:00
* 2:00
* 3:00


### example 02b :

``* 1-3 * * * echo 1``

means : run EVERY minute of hours 1, 2 and 3 .. so it will run on every minute 1:00 to 3:59 (3 * 60 = 180 times)


### example 03 :

``0 0 1 * * echo 1``

means : run on a 1st day of a month


### example 04 :

``0 0 1 1,3 * echo 1``

means : run on start of january and march .. so :
* 1st of january on 0:00
* 1st of march on 0:00


### example 05 :

``0 0 * * 3 echo 1``

means : run on wednesday

